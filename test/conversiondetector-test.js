'use strict';
var path = require("path");
var chai = require("chai");
chai.should();
chai.use(require('chai-things'));

var sinon = require('sinon');
var rewire = require("rewire");
var ConversionDetector = rewire("../src/lib/conversiondetector");
var acornGlobals = require('acorn-globals');

var expect = chai.expect;

describe("ConversionDetector", function() {
    beforeEach(function() {
        var self = this;

        this.searchDir = "./test/data";
        this.baseConfig = {symbols: {OK: "o", ERR: "x", ALERT: "!"}};
        this.baseArgv = {globals: false};
        // Generated via https://astexplorer.net/
        // With the code:
        // ```
        // one /////////
        // two //////////
        // three
        // ```
        this.nodes = [{"type": "ExpressionStatement", "start": 0, "end": 3, "range": [0, 3 ], "expression": {"type": "Identifier", "start": 0, "end": 3, "range": [0, 3 ], "name": "one"} }, {"type": "ExpressionStatement", "start": 14, "end": 17, "range": [14, 17 ], "expression": {"type": "Identifier", "start": 14, "end": 17, "range": [14, 17 ], "name": "two"} }, {"type": "ExpressionStatement", "start": 29, "end": 34, "range": [29, 34 ], "expression": {"type": "Identifier", "start": 29, "end": 34, "range": [29, 34 ], "name": "three"} } ];
        this.nodes.forEach(function(node) {
            node.expression.parents = [node.expression, node];
            node.parents = [node];
        });

        this.acorn = {getLineInfo: sinon.stub()};
        this.stubbedDetectAMD = sinon.stub();
        this.stubbedDetectGlobals = sinon.stub();
        this.glob = {sync: sinon.stub()};
        this.fs = {readFileSync: sinon.stub()};
        this.stubbedInfoLogger = sinon.stub();
        this.stubbedWarnLogger = sinon.stub();
        this.stubbedErrorLogger = sinon.stub();
        var winston = {
            transports: sinon.stub(),
            Logger: function() {
                this.info = self.stubbedInfoLogger;
                this.warn = self.stubbedWarnLogger;
                this.error = self.stubbedErrorLogger;
                this.add = sinon.stub();
            }
        };
        sinon.createStubInstance(winston.Logger);

        ConversionDetector.__set__({
            'detectAMD': this.stubbedDetectAMD,
            'detectGlobals': this.stubbedDetectGlobals,
            'winston': winston,
            "glob": this.glob,
            "fs": this.fs,
            "acorn": this.acorn
        });

        this.detector = new ConversionDetector(this.searchDir, this.baseConfig, this.baseArgv);
    });

    describe("setupExcludedGlobals", function() {
        it("should populate the excluded globals array from 'env'", function() {
            var envs = {"amd": true};

            var excludes = this.detector.setupExcludedGlobals(envs);

            expect(excludes).to.contain("define");
            expect(excludes).to.contain("require");
        });
    });

    describe("getFilename", function() {
        it("should get the filename from a file path", function() {
            var fullFilePath = "/foo/bar/index.js";

            var filename = this.detector.getFilename(fullFilePath);

            expect(filename).to.equal("index.js");
        });
    });

    describe("markAsNonAMD", function() {
        it("should increment the 'non AMD module' counter", function() {
            sinon.stub(this.detector, "getFilename");
            expect(this.detector._countNonAMD).to.equal(0);

            this.detector.markAsNonAMD("/foo/bar/index.js");

            expect(this.detector._countNonAMD).to.equal(1);
        });

        it("should log a line", function() {
            var filename = "index.js";
            sinon.stub(this.detector, "getFilename").returns(filename);

            this.detector.markAsNonAMD("/foo/bar/index.js");
            this.detector.printFileSummary();

            expect(this.stubbedInfoLogger.getCall(0).args[0]).to.have.string("/foo/bar");
            expect(this.stubbedErrorLogger.getCall(0).args[0]).to.have.string(filename).and.to.have.string("Not an AMD module");
        });
    });

    describe("markAsAMD", function() {
        it("should increment the 'AMD define' counter", function() {
            sinon.stub(this.detector, "findGlobals");
            expect(this.detector._countAMDDefine).to.equal(0);

            this.detector.markAsAMD("/foo/bar/index.js", "");

            expect(this.detector._countAMDDefine).to.equal(1);
        });

        it("should initiate the finding of globals", function() {
            var fullFilePath = "/foo/bar/index.js";
            var content = "var x = 0;";
            sinon.stub(this.detector, "findGlobals");

            this.detector.markAsAMD(fullFilePath, content);

            sinon.assert.calledWith(this.detector.findGlobals, fullFilePath, content);
        });
    });

    describe("markAsAMDRequire", function() {
        it("should increment the 'AMD require' counter", function() {
            sinon.stub(this.detector, "findGlobals");
            expect(this.detector._countAMDRequire).to.equal(0);

            this.detector.markAsAMDRequire("/foo/bar/index.js", "");

            expect(this.detector._countAMDRequire).to.equal(1);
        });

        it("should initiate the finding of globals", function() {
            var fullFilePath = "/foo/bar/index.js";
            var content = "var x = 0;";
            sinon.stub(this.detector, "findGlobals");

            this.detector.markAsAMDRequire(fullFilePath, content);

            sinon.assert.calledWith(this.detector.findGlobals, fullFilePath, content);
        });
    });

    describe("trimToEndOfVariableName", function() {
        it("should trim off any spaces", function() {
            var globals = acornGlobals(" Foo.Bar.baz   ");
            console.log(globals.map(g => g.name + ": { nodes: " + g.nodes.map(n => n.name + ":" + n.type + ": { parents: " + n.parents.map(p => p.name + ":" + p.type) + " }") + " }"));
            expect(globals.length).to.equal(1);
            expect(globals[0].name).to.equal("Foo");
            expect(globals[0].nodes.length).to.equal(1);
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a newline", function() {
            var globals = acornGlobals("Foo.Bar.baz\nvar x = 0;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a (", function() {
            var globals = acornGlobals("Foo.Bar.baz(x,y,z);");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a )", function() {
            var globals = acornGlobals("(true,Foo.Bar.baz);var x = 0;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a [", function() {
            var globals = acornGlobals("Foo.Bar.baz['x'] = 0;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz.x");
        });

        it("should trim at a }", function() {
            var globals = acornGlobals("{a:Foo.Bar.baz};var x = 0;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a ,", function() {
            var globals = acornGlobals("Foo.Bar.baz,x = 0;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a ;", function() {
            var globals = acornGlobals("Foo.Bar.baz;var x = 0;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a |", function() {
            var globals = acornGlobals("Foo.Bar.baz|x;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });

        it("should trim at a =", function() {
            var globals = acornGlobals("Foo.Bar.baz = 0;");
            var result = this.detector.trimToEndOfVariableName(globals[0].nodes[0]);

            expect(result).to.equal("Foo.Bar.baz");
        });
    });

    describe("isExcludedVariable", function() {
        it("should test a variable against the list of excluded variables from configuration", function() {
            var config = {exclude: {variables: ['AJS.contextPath', 'AJS.namespace', 'AJS.I18n.getText', 'A.B']}};
            var detector = new ConversionDetector(this.searchDir, config, this.baseArgv);

            expect(detector.isExcludedVariable("Foo"), "Foo to be included").to.be.not.ok;
            expect(detector.isExcludedVariable("AJS.contextPath"), "AJS.contextPath to be excluded").to.be.ok;
            expect(detector.isExcludedVariable("AJS.namespace"), "AJS.namespace to be excluded").to.be.ok;
            expect(detector.isExcludedVariable("AJS.I18n.getText"), "AJS.I18n.getText to be excluded").to.be.ok;
            expect(detector.isExcludedVariable("A"), "A to be included").to.be.not.ok;
            expect(detector.isExcludedVariable("A.B"), "A.B to be excluded").to.be.ok;
            expect(detector.isExcludedVariable("A.B.C"), "A.B.C to be excluded").to.be.ok;
        });
    });

    describe("isAMDDefine", function() {
        it("detects objects with a type of 'define' as AMD calls", function() {
            expect(this.detector.isAMDDefine({name: "test", type:"define"}), "should be valid").to.be.ok;
            expect(this.detector.isAMDDefine({type: "define"}), "should be valid").to.be.ok;
            expect(this.detector.isAMDDefine({deps: []}), "should not be a module").to.be.not.ok;
        });
    });

    describe("isAMDRequire", function() {
        it("detects objects with a type of 'require' as AMD calls", function() {
            expect(this.detector.isAMDRequire({type: "require", deps: ["foo"]}), "should be valid").to.be.ok;
            expect(this.detector.isAMDRequire({type: "require"}), "should be valid").to.be.ok;
            expect(this.detector.isAMDRequire({deps: ["foo"]}), "should not be valid").to.be.not.ok;
        });
    });

    describe("isNamedAMDModule", function() {
        it("should test whether the parameter represents a named AMD module", function() {
            expect(this.detector.isNamedAMDModule({type:"define", name: "test", deps:[]}), "to be a valid AMD module representation").to.be.ok;
            expect(this.detector.isNamedAMDModule({type:"define", name: "test"}), "not to be a valid AMD module representation").to.be.ok;
            expect(this.detector.isNamedAMDModule({type:"define", deps: []}), "not to be a valid AMD module representation").to.be.not.ok;

            expect(this.detector.isNamedAMDModule({name: "test", deps:[]}), "to be a valid AMD module representation").to.be.not.ok;
            expect(this.detector.isNamedAMDModule({name: "test"}), "not to be a valid AMD module representation").to.be.not.ok;
            expect(this.detector.isNamedAMDModule({deps: []}), "not to be a valid AMD module representation").to.be.not.ok;
        });
    });

    describe("getRelevantGlobals", function() {
        it("should return a list of relevant globals", function() {
            this.detector._excludedGlobals = ['Excluded'];
            var globals = [
                {name: "Foo", nodes: [{},{},{}]},
                {name: "Bar", nodes: [{},{}]},
                {name: "Baz", nodes: []},
                {name: "Excluded", nodes: [{},{},{},{}]}
            ];
            var content = "foo";
            this.stubbedDetectGlobals.withArgs(content).returns(globals);
            sinon.stub(this.detector, "getRelevantOccurrences").returnsArg(1);

            var relevantGlobals = this.detector.getRelevantGlobals(content);

            sinon.assert.calledWith(this.stubbedDetectGlobals, content);
            relevantGlobals.should.include.something.that.deep.equals(globals[0]);
            relevantGlobals.should.include.something.that.deep.equals(globals[1]);
            relevantGlobals.should.not.include.something.that.deep.equals(globals[2]);
            relevantGlobals.should.not.include.something.that.deep.equals(globals[3]);

        });
    });

    describe("getRelevantOccurrences", function() {
        it("should return empty list when there are no nodes", function() {
            var occurrences = this.detector.getRelevantOccurrences("var x = 0;", null);

            expect(occurrences).to.deep.equal([]);
        });

        it("should return a list filtered from excluded variables", function() {
            sinon.stub(this.detector, "trimToEndOfVariableName")
                .onFirstCall().returns("one")
                .onSecondCall().returns("two")
                .onThirdCall().returns("three");
            sinon.stub(this.detector, "isExcludedVariable")
                .returns(false)
                .withArgs("three").returns(true);

            var occurrences = this.detector.getRelevantOccurrences("", this.nodes);

            occurrences.should.include.something.that.deep.equals(this.nodes[0]);
            occurrences.should.include.something.that.deep.equals(this.nodes[1]);
            occurrences.should.not.include.something.that.deep.equals(this.nodes[2]);
        });

        it("should increment the globals/occurrences counter", function() {
            sinon.stub(this.detector, "isExcludedVariable").returns(false);

            this.detector.getRelevantOccurrences("", this.nodes);

            expect(this.detector._countGlobals).to.equal(3);
        });
    });

    describe("processRelevantOccurrences", function() {
        it("should prepare a log line on each occurrence", function() {
            this.acorn.getLineInfo.returns({line: 0, column: 0});
            sinon.stub(this.detector, "isExcludedVariable").returns(false);

            var result = this.detector.processRelevantOccurrences("", this.nodes);

            expect(result).to.have.length(3);
        });

        it("should print the correct variable names", function() {
            sinon.stub(this.detector, "trimToEndOfVariableName")
                .onFirstCall().returns("one")
                .onSecondCall().returns("two")
                .onThirdCall().returns("three");
            sinon.stub(this.detector, "isExcludedVariable").returns(false);
            this.acorn.getLineInfo.returns({line: 0, column: 0});

            var result = this.detector.processRelevantOccurrences("", this.nodes);

            expect(result[0]).to.have.property("varName", "one");
            expect(result[1]).to.have.property("varName", "two");
            expect(result[2]).to.have.property("varName", "three");
        });

        it("should print the correct line numbers", function() {
            this.acorn.getLineInfo
                .withArgs(sinon.match.string, this.nodes[0].start).returns({line: 13, column: 0})
                .withArgs(sinon.match.string, this.nodes[1].start).returns({line: 1337, column: 666})
                .withArgs(sinon.match.string, this.nodes[2].start).returns({line: 999, column: 123});
            sinon.stub(this.detector, "isExcludedVariable").returns(false);

            var result = this.detector.processRelevantOccurrences("", this.nodes);

            expect(result[0]).to.have.nested.property("lineInfo.line", 13);
            expect(result[0]).to.have.nested.property("lineInfo.column", 0);
            expect(result[1]).to.have.nested.property("lineInfo.line", 1337);
            expect(result[1]).to.have.nested.property("lineInfo.column", 666);
            expect(result[2]).to.have.nested.property("lineInfo.line", 999);
            expect(result[2]).to.have.nested.property("lineInfo.column", 123);
        });
    });

    describe("findGlobals", function() {
        it("should print occurrences when 'globals' config value is set", function() {
            var detector = new ConversionDetector(this.searchDir, this.baseConfig, {globals: true});
            var globals = [{},{},{}];
            sinon.stub(detector, "getRelevantGlobals").returns(globals);
            sinon.spy(detector, "processRelevantOccurrences");

            detector.findGlobals("");

            expect(detector.processRelevantOccurrences.callCount).to.equal(globals.length);
        });

        it("should print occurrences when 'idea-globals' config value is set", function() {
            var detector = new ConversionDetector(this.searchDir, this.baseConfig, {"idea-globals": true});
            var globals = [{},{},{}];
            sinon.stub(detector, "getRelevantGlobals").returns(globals);
            sinon.spy(detector, "processRelevantOccurrences");

            detector.findGlobals("");

            expect(detector.processRelevantOccurrences.callCount).to.equal(globals.length);
        });

        it("should not print occurrences when no 'globals' or 'idea-globals' config value is set", function() {
            sinon.stub(this.detector, "getRelevantGlobals").returns([{},{},{}]);
            sinon.spy(this.detector, "processRelevantOccurrences");

            this.detector.findGlobals("");

            expect(this.detector.processRelevantOccurrences.callCount).to.equal(0);
        });
    });

    describe("findGlobals - in print", function() {
        it("should log an 'OK' line when no globals found", function() {
            sinon.stub(this.detector, "getRelevantGlobals").returns([]);

            this.detector.findGlobals("/foo/bar/index.js");
            this.detector.printFileSummary();

            expect(this.stubbedInfoLogger.getCall(0).args[0]).to.have.string("/foo/bar");
            expect(this.stubbedInfoLogger.getCall(1).args[0])
                .to.have.string("index.js").and
                .to.have.string(this.baseConfig.symbols.OK);
        });
        it("should log an 'ALERT' line when a global is found", function() {
            sinon.stub(this.detector, "getRelevantGlobals").returns([{},{},{}]);

            this.detector.findGlobals("/foo/bar/index.js");
            this.detector.printFileSummary();

            expect(this.stubbedInfoLogger.getCall(0).args[0]).to.have.string("/foo/bar");
            expect(this.stubbedWarnLogger.getCall(0).args[0])
                .to.have.string("index.js").and
                .to.have.string(this.baseConfig.symbols.ALERT);
        });
        it("should log an 'ALERT' line when the content can't be parsed", function() {
            sinon.stub(this.detector, "getRelevantGlobals").throws();

            this.detector.findGlobals("/foo/bar/index.js");
            this.detector.printFileSummary();

            expect(this.stubbedInfoLogger.getCall(0).args[0]).to.have.string("/foo/bar");
            expect(this.stubbedErrorLogger.getCall(0).args[0])
                .to.have.string("index.js").and
                .to.have.string(this.baseConfig.symbols.ERR);
        });
        it("should prepare a line for each 'occurrence' of a global that is found", function() {
            var detector = new ConversionDetector(this.searchDir, this.baseConfig, {globals: true});
            sinon.stub(detector, "getRelevantGlobals").returns([{},{},{}]);
            sinon.stub(detector, "processRelevantOccurrences");

            detector.findGlobals("/foo/bar/index.js", "");
            this.detector.printFileSummary();

            sinon.assert.calledThrice(detector.processRelevantOccurrences);
        });
    });

    describe("handleFile", function() {
        var content = "foo";
        var directory = "/foo/bar";
        var filename = "index.js";
        var fullFilePath = path.join(directory, filename);
        var namedModuleStub;
        // eslint-disable-next-line no-unused-vars
        var markAsAmdStub;
        // eslint-disable-next-line no-unused-vars
        var markAsNonAmdStub;
        var next;

        beforeEach(function() {
            namedModuleStub = sinon.stub(this.detector, "isNamedAMDModule");
            markAsAmdStub = sinon.stub(this.detector, "markAsAMD");
            markAsNonAmdStub = sinon.stub(this.detector, "markAsNonAMD");
            next = sinon.spy();
        });

        it("should pass the file contents to the AMD detection library", function() {
            namedModuleStub.returns(true);

            this.detector.handleFile(null, content, "", next);

            sinon.assert.calledWith(this.stubbedDetectAMD, content);
        });

        it("should make sure the next file will be handled", function() {
            this.detector.handleFile(null, "", "", next);

            sinon.assert.calledOnce(next);
        });

        describe("when marking content", function() {
            var amdModuleResults = [{name: "foo/bar/baz", deps: []}];
            var firstNode = amdModuleResults[0];

            beforeEach(function() {
                this.stubbedDetectAMD.withArgs(content).returns(amdModuleResults);
            });

            it("should be given the first node from a file", function() {
                this.detector.handleFile(null, content, fullFilePath, next);

                sinon.assert.calledWith(this.detector.isNamedAMDModule, firstNode);
            });

            it("should mark the file as an AMD module", function() {
                namedModuleStub.returns(true);

                this.detector.handleFile(null, content, fullFilePath, next);

                sinon.assert.calledWith(this.detector.markAsAMD, fullFilePath, content);
            });

            it("should mark the file as a non AMD module", function() {
                namedModuleStub.returns(false);

                this.detector.handleFile(null, "", fullFilePath, next);

                sinon.assert.calledWith(this.detector.markAsNonAMD, fullFilePath);
            });
        });
    });

    describe("printFileSummary", function() {
        var detector;
        // eslint-disable-next-line no-unused-vars
        var namedModuleStub;
        // eslint-disable-next-line no-unused-vars
        var relevantGlobalsStub;
        var next;

        beforeEach(function() {
            detector = new ConversionDetector(this.searchDir, this.baseConfig, {});
            relevantGlobalsStub = sinon.stub(detector, "getRelevantGlobals").returns([]);
            namedModuleStub = sinon.stub(detector, "isNamedAMDModule").returns(true);
            next = sinon.spy();
        });

        it("should log a line on every new directory", function() {
            detector.handleFile(null, "", "foo/bar/a.js", next);
            detector.handleFile(null, "", "foo/bar/b.js", next);
            detector.printFileSummary();

            sinon.assert.calledWith(this.stubbedInfoLogger, "foo/bar");
        });

        it("should log directories and files in alphabetical order", function() {
            detector.handleFile(null, "", "/zeta/zeta.js", next);
            detector.handleFile(null, "", "/foo/bar/b.js", next);
            detector.handleFile(null, "", "/foo/bar/d.js", next);
            detector.handleFile(null, "", "/alpha/alpha.js", next);
            detector.handleFile(null, "", "/foo/bar/c.js", next);
            detector.handleFile(null, "", "/foo/bar/a.js", next);
            detector.handleFile(null, "", "/gamma/gamma.js", next);
            detector.printFileSummary();

            var report = this.stubbedInfoLogger.getCalls();
            expect(report[0].args[0]).to.have.string("/alpha");
            expect(report[1].args[0]).to.have.string(" alpha.js");
            expect(report[2].args[0]).to.have.string("/foo/bar");
            expect(report[3].args[0]).to.have.string(" a.js");
            expect(report[4].args[0]).to.have.string(" b.js");
            expect(report[5].args[0]).to.have.string(" c.js");
            expect(report[6].args[0]).to.have.string(" d.js");
            expect(report[7].args[0]).to.have.string("/gamma");
            expect(report[8].args[0]).to.have.string(" gamma.js");
            expect(report[9].args[0]).to.have.string("/zeta");
            expect(report[10].args[0]).to.have.string(" zeta.js");
        });

        it("should log an 'ALERT' line when unable to parse the file", function() {
            var error = {message: "Y U NO WORK"};
            detector.handleFile(error, "", "/foo/bar/error.js", next);
            detector.handleFile(null, "", "/foo/bar/index.js", next);
            detector.printFileSummary();

            expect(this.stubbedInfoLogger.getCall(0).args[0]).to.have.string("/foo/bar");
            expect(this.stubbedErrorLogger.getCall(0).args[0])
                .to.have.string("error.js").and
                .to.have.string(this.baseConfig.symbols.ERR).and
                .to.have.string(error.message);
        });

        it("should not output already converted files", function() {

        });
    });

    describe("printGlobalsSummary", function() {
        var makeDetector;

        beforeEach(function() {
            var baseConfig = this.baseConfig;
            var searchDir = this.searchDir;
            makeDetector = function(globalSummaryOn) {
                return new ConversionDetector(searchDir, baseConfig, {
                    'files': false,
                    'globals-summary': !!globalSummaryOn
                });
            };
        });

        it("does not get output if globals-summary was not set", function () {
            var detector = makeDetector(false);
            detector.printGlobalsSummary();
            expect(this.stubbedInfoLogger.callCount).to.equal(0);
        });

        it("should print in the order they are used", function () {
            var detector = makeDetector(true);
            detector._globals = [{varName: "foo"}, {varName:"bar"}, {varName:"bar"}, {varName: "bar"}];
            detector.printGlobalsSummary();

            expect(this.stubbedInfoLogger.args[0][0]).to.include("foo").and.to.include("1");
            expect(this.stubbedInfoLogger.args[1][0]).to.include("bar").and.to.include("3");
        });

        it("should sort alphabetically when usage count is equal", function () {
            var detector = makeDetector(true);
            detector._globals = [{varName: "foo"}, {varName:"bar"}, {varName:"baz"}, {varName: "alpha"}];
            detector.printGlobalsSummary();

            expect(this.stubbedInfoLogger.args[0][0]).to.include("alpha").and.to.include("1");
            expect(this.stubbedInfoLogger.args[1][0]).to.include("bar").and.to.include("1");
            expect(this.stubbedInfoLogger.args[2][0]).to.include("baz").and.to.include("1");
            expect(this.stubbedInfoLogger.args[3][0]).to.include("foo").and.to.include("1");
        });
    });

    describe("printSummary", function() {
        it("should print a summary", function() {
            var detector = new ConversionDetector(this.searchDir, this.baseConfig, {});

            detector.printSummary();

            sinon.assert.callCount(this.stubbedInfoLogger, 5);
            sinon.assert.callCount(this.stubbedWarnLogger, 2);
        });
        it("should not print a summary when --no-summary is set", function() {
            var detector = new ConversionDetector(this.searchDir, this.baseConfig, {summary: false});

            detector.printSummary();

            sinon.assert.notCalled(this.stubbedInfoLogger);
            sinon.assert.notCalled(this.stubbedWarnLogger);
        });
    });

    describe("getGlobs", function() {
        it("should prefix all globs with the search directory", function() {
            var config = {patterns: ['foo'], exclude: {patterns: ['bar','baz']}};
            var detector = new ConversionDetector(this.searchDir, config, this.baseArgv);

            var globList = detector.getGlobs(this.searchDir);

            expect(globList[0]).to.have.string(this.searchDir).and.to.have.string('foo');
            expect(globList[1]).to.have.string(this.searchDir).and.to.have.string('bar');
            expect(globList[2]).to.have.string(this.searchDir).and.to.have.string('baz');
        });
        it("should prefix excludes with an exclamation mark", function() {
            var config = {patterns: ['foo'], exclude: {patterns: ['bar', 'baz']}};
            var detector = new ConversionDetector(this.searchDir, config, this.baseArgv);

            var globList = detector.getGlobs(this.searchDir);

            expect(globList[1]).to.match(/^!/).and.to.have.string('bar');
            expect(globList[2]).to.match(/^!/).and.to.have.string('baz');
        });
        it("should return the list of globs", function() {
            var config = {patterns: ['foo'], exclude: {patterns: ['bar', 'baz']}};
            var detector = new ConversionDetector(this.searchDir, config, this.baseArgv);

            var globList = detector.getGlobs(this.searchDir);

            expect(globList).to.have.length(3);
            expect(globList[0]).to.have.string('foo');
            expect(globList[1]).to.have.string('bar');
            expect(globList[2]).to.have.string('baz');
        });
    });

    describe("run", function() {
        it("should setup excluded globals", function() {
            var excludedGlobals = ['foo','bar'];
            var config = {exclude: {directories: []}, symbols: this.baseConfig.symbols};
            var detector = new ConversionDetector(this.searchDir, config, this.baseArgv);
            sinon.stub(detector, "setupExcludedGlobals").returns(excludedGlobals);

            this.glob.sync.returns([]);

            detector.run();

            sinon.assert.calledOnce(detector.setupExcludedGlobals);
            expect(detector._excludedGlobals).to.deep.equal(excludedGlobals);
        });
        it("should search the search folder for files", function() {
            var globs = ['../foo/bar/**/*.js'];
            var matches = ['foo.js','bar.js'];
            sinon.stub(this.detector, "getGlobs").returns(globs);
            sinon.stub(this.detector, "handleFile");
            this.glob.sync.returns(matches);

            this.detector.run();

            sinon.assert.calledWith(this.glob.sync, sinon.match(globs));
            sinon.assert.calledWith(this.fs.readFileSync, sinon.match(matches[0]));
            sinon.assert.calledWith(this.fs.readFileSync, sinon.match(matches[1]));
            sinon.assert.calledTwice(this.detector.handleFile);
        });
    });
});
