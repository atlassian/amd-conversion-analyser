# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.8.0] - 2021-04-13
### Changed

* Bump Acorn v5.7.3 -> v8.1.0

## [1.7.1] - 2020-11-02
### Changed

* Bump Acorn v3.0.4 to v5.7.3
* Can parse EcmaScript 2017 (ES8) syntax
